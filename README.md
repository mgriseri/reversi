## Description

An implementation of the [Reversi / Othello board game](https://en.wikipedia.org/wiki/Reversi)
written in the [Nim programming language](https://nim-lang.org/).

It uses the [minimax algorithm](https://en.wikipedia.org/wiki/Minimax)
with [alpha-beta pruning](https://en.wikipedia.org/wiki/Alpha%E2%80%93beta_pruning)
along with a fairly simple static evaluation function.

The evaluation takes into account:

- Mobility (especially in the opening)
- Positional play (especially in the end-game)

## Usage

Launch the program from the terminal.
You'll see an ASCII representation of the board,
some information about the game, and a prompt.

Enter a legal move (using coordinates)
or enter "move" or "m" to let the computer play.
When you or the computer must pass, you have to enter "pass" or "p".

To change the depth of the computer,
enter "depth" followed by an integer (default is 3).
(Don't try this at home: larger values take significantly more time)

If you want to quit, enter "quit" or "q".

## Strength

I tested it (with the default 3 ply depth) against
[GNOME Reversi](https://wiki.gnome.org/Apps/Reversi) in "hard" mode,
just for reference. I was actually surprised how often it won!

It's not very fast, and there are [better software out there](https://www.ffothello.org/informatique/telecharger-des-programmes/) (page in french),
but it should be quite challenging for any casual player.

## Building

Assuming you have the Nim compiler installer, just execute:

    nim c -d:release --opt:speed reversi.nim
