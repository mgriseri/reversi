import re

from algorithm import sort
from os import execShellCmd
from random import randomize, shuffle
from sequtils import deduplicate, zip
from strformat import fmt
from strutils import parseInt, split

const LETTERS = "abcdefgh"
const DIRECTIONS = ["n", "ne", "e", "se", "s", "sw", "w", "nw"]

# Weights of the squares for positional evaluation
# Source: "Othello/Reversi using Game Theory techniques"
# By: Parth Parekh, Urjit Singh Bhatia & Kushal Sukthankar

const WEIGHTS = @[
  @[100, -20, 10, 5, 5, 10, -20, 100],
  @[-20, -50, -2, -2, -2, -2, -50, -20],
  @[10, -2, -1, -1, -1, -1, -2, 10],
  @[5, -2, -1, -1, -1, -1, -2, 5],
  @[5, -2, -1, -1, -1, -1, -2, 5],
  @[10, -2, -1, -1, -1, -1, -2, 10],
  @[-20, -50, -2, -2, -2, -2, -50, -20],
  @[100, -20, 10, 5, 5, 10, -20, 100]
]

const STARTING_POSITION = @[
  @[0, 0, 0, 0, 0, 0, 0, 0],
  @[0, 0, 0, 0, 0, 0, 0, 0],
  @[0, 0, 0, 0, 0, 0 ,0, 0],
  @[0, 0, 0, -1, 1, 0, 0, 0],
  @[0, 0, 0, 1, -1, 0, 0, 0],
  @[0, 0, 0, 0, 0, 0, 0, 0],
  @[0, 0, 0, 0, 0, 0, 0, 0],
  @[0, 0, 0, 0, 0, 0, 0, 0]
]

proc fromString(move: string): seq[int] =
  let x = LETTERS.find(move[0])
  let y = fmt"{move[1]}".parseInt - 1

  return @[y, x]

proc toString(move: seq[int]): string =
  let x = move[1]
  let y = move[0]

  return fmt"{LETTERS[x]}{y + 1}"

proc drawBoard(position: seq[seq[int]]) =
  echo "    a  b  c  d  e  f  g  h  "
  for irow in 0..7:
    stdout.write(irow + 1)
    stdout.write(" ")
    let row = position[irow]
    stdout.write("|")
    for square in row:
      if square == -1:
        stdout.write(" W ")
      if square == 0:
        stdout.write(" _ ")
      if square == 1:
        stdout.write(" B ")
    stdout.write("|\n")

proc getNeighbor(coordinate: seq[int], direction: string): seq[int] =
  var neighbor: seq[int]
  case direction

  of "n":
    if coordinate[0] > 0:
      neighbor = @[coordinate[0] - 1, coordinate[1]]

  of "ne":
    if coordinate[0] > 0 and coordinate[1] < 7:
      neighbor = @[coordinate[0] - 1, coordinate[1] + 1]

  of "e":
    if coordinate[1] < 7:
      neighbor = @[coordinate[0], coordinate[1] + 1]

  of "se":
    if coordinate[0] < 7 and coordinate[1] < 7:
      neighbor = @[coordinate[0] + 1, coordinate[1] + 1]

  of "s":
    if coordinate[0] < 7:
      neighbor = @[coordinate[0] + 1, coordinate[1]]

  of "sw":
    if coordinate[0] < 7 and coordinate[1] > 0:
      neighbor = @[coordinate[0] + 1, coordinate[1] - 1]

  of "w":
    if coordinate[1] > 0:
      neighbor = @[coordinate[0], coordinate[1] - 1]

  of "nw":
    if coordinate[0] > 0 and coordinate[1] > 0:
      neighbor = @[coordinate[0] - 1, coordinate[1] - 1]

  return neighbor

proc getLine(coordinate: seq[int], direction: string): seq[seq[int]] =
  var neighbor = getNeighbor(coordinate, direction)
  var line = @[
    neighbor
  ]

  while neighbor.len > 0:
    neighbor = getNeighbor(neighbor, direction)
    if neighbor.len > 0:
      line.add(neighbor)
  return line

proc getEmptySquares(position: seq[seq[int]]): seq[seq[int]] =
  var emptySquares: seq[seq[int]]

  for irow in 0..7:
    for icolumn in 0..7:
      if position[irow][icolumn] == 0:
        let emptySquare = @[irow, icolumn]
        emptySquares.add(emptySquare)

  return emptySquares

proc getLegalMoves(position: seq[seq[int]], player: int): seq[seq[int]] =
  var legalMoves: seq[seq[int]] 

  let emptySquares = getEmptySquares(position)

  for square in emptySquares:
    for direction in DIRECTIONS:
      let neighbor = getNeighbor(square, direction)
      let hasNeighbor = neighbor.len > 0

      if not hasNeighbor:
        continue

      let enemy = player * -1
      let hasEnemyNeighbor = position[neighbor[0]][neighbor[1]] == enemy

      let line = getLine(square, direction)
      var hasPlayer = false

      for otherSquare in line:
        let isPlayer = position[otherSquare[0]][otherSquare[1]] == player
        let isEmpty = position[otherSquare[0]][otherSquare[1]] == 0

        if isEmpty:
          break

        if isPlayer:
          hasPlayer = true
          break

      if hasEnemyNeighbor and hasPlayer:
        legalMoves.add(square)

  return deduplicate(legalMoves)

proc isLegalMove(position: seq[seq[int]], player: int, move: seq[int]): bool =
  let legalMoves = getLegalMoves(position, player)

  var isLegal = false
  for legalMove in legalMoves:
    if move[0] == legalMove[0] and move[1] == legalMove[1]:
      isLegal = true
      break  

  return isLegal

proc isGameOver(position: seq[seq[int]], player: int): bool =
  let hasNoMovePlayer = getLegalMoves(position, player).len == 0
  let hasNoMoveEnemy = getLegalMoves(position, player * -1).len == 0

  return hasNoMovePlayer and hasNoMoveEnemy

proc makeMove(position: seq[seq[int]], player: int, move: seq[int]): seq[seq[int]] =
  if not isLegalMove(position, player, move):
    return position

  var newPosition = deepCopy(position)
  newPosition[move[0]][move[1]] = player

  var directionsAffectedByMove: seq[string]

  for direction in DIRECTIONS:
    let neighbor = getNeighbor(@[move[0], move[1]], direction)
    let hasNeighbor = neighbor.len > 0

    if not hasNeighbor:
      continue 

    let enemy = player * -1
    var hasEnemyNeighbor = position[neighbor[0]][neighbor[1]] == enemy

    let line = getLine(@[move[0], move[1]], direction)
    var hasPlayer = false

    for otherSquare in line:
      let isPlayer = position[otherSquare[0]][otherSquare[1]] == player
      let isEmpty = position[otherSquare[0]][otherSquare[1]] == 0

      if isEmpty:
        break

      if isPlayer:
        hasPlayer = true
        break

    if hasEnemyNeighbor and hasPlayer:
      directionsAffectedByMove.add(direction)

  for direction in directionsAffectedByMove:
    let line = getLine(@[move[0], move[1]], direction)
    for square in line:
      if newPosition[square[0]][square[1]] == player:
        break
      newPosition[square[0]][square[1]] *= -1

  return newPosition

proc materialScore(position: seq[seq[int]]): int =
  var balance: int

  for row in position:
    for square in row:
      balance += square

  return balance

proc mobilityScore(position: seq[seq[int]]): int =
  let nbOfMovesForBlack = getLegalMoves(position, 1).len
  let nbOfMovesForWhite = getLegalMoves(position, -1).len

  return nbOfMovesForBlack - nbOfMovesForWhite

proc positionalScore(position: seq[seq[int]]): int =
  var score: int

  for irow in 0..7:
    for icolumn in 0..7:
      let weight = WEIGHTS[irow][icolumn]
      let square = position[irow][icolumn]
      score += weight * square

  return score

proc evaluate(position: seq[seq[int]]): int =
  # Weights for the different scores:
  # Because positionalScore uses a wide range of numbers,
  # mobilityScore needs to be bigger
  # to have any significance in the final evaluation

  var mobilityW = 10  # greater = prefers positions with more moves
  var positionalW = 1  # greater = more focused on square weights

  # the first 15 moves (60 - 15 = 45)
  let isOpening = getEmptySquares(position).len > 45
  # the last 20 moves
  let isEndGame = getEmptySquares(position).len < 20

  if isOpening:
    mobilityW = 20

  if isEndGame:
    mobilityW = 0

  let mobilityScore = mobilityW * mobilityScore(position)
  let positionalScore = positionalW * positionalScore(position)

  return mobilityScore + positionalScore

proc minMax(position: seq[seq[int]], player: int, depth: int, alpha:int = -999, beta:int = 999): int =
  # MinMax algorithm from: https://www.youtube.com/watch?v=l-hh51ncgDI

  var alpha = alpha
  var beta = beta

  if depth == 0 or isGameOver(position, player):
    return evaluate(position)

  if player == 1:
    var maxEval = -999
    for move in getLegalMoves(position, player):
      let eval = minMax(makeMove(position, player, move), -1, depth - 1, alpha, beta)
      maxEval = if eval > maxEval: eval else: maxEval
      alpha = if eval > alpha: eval else: alpha
      if beta <= alpha:
        break

    return maxEval

  if player == -1:
    var minEval = 999
    for move in getLegalMoves(position, player):
      let eval = minMax(makeMove(position, player, move), 1, depth - 1, alpha, beta)
      minEval = if eval < minEval: eval else: minEval
      beta = if eval < beta: eval else: beta
      if beta <= alpha:
        break

    return minEval

proc getBestMove(position: seq[seq[int]], player: int, depth: int): seq[int] =
  var legalMoves = getLegalMoves(position, player)

  randomize()
  legalMoves.shuffle()

  var legalMovesEvals: seq[int]

  echo "Thinking..."
  for move in legalMoves:
    let eval = minMax(makeMove(position, player, move), player * -1, depth)
    echo fmt"{move.toString}: {eval}"
    legalMovesEvals.add(eval)

  var legalMovesWithEvals = zip(legalMoves, legalMovesEvals)

  proc byEval(x, y: tuple[move: seq[int], eval: int]): int =
    let eval = x[1]
    let evalNext = y[1]

    if eval < evalNext:
      return -1
    elif eval > evalNext:
      return 1
    else:
      return 0

  legalMovesWithEvals.sort(byEval)

  let bestMoveForWhite = legalMovesWithEvals[0][0]
  let bestMoveForBlack = legalMovesWithEvals[legalMovesWithEvals.len - 1][0]

  if player == -1:
    return bestMoveForWhite
  else:
    return bestMoveForBlack

proc main() =
  var position = STARTING_POSITION
  var player = 1
  var isPlaying = true
  var depth = 3

  var moveHistory: seq[string]

  while isPlaying:
    discard execShellCmd("clear")
    drawBoard(position)

    stdout.write("\n")
    if player == 1:
      echo "Black to move"
    if player == -1:
      echo "White to move"

    echo fmt"Material: {materialScore(position)}"

    echo "Move history:"
    if moveHistory.len < 1:
      stdout.write("...")
    for move in moveHistory:
      stdout.write(fmt"{move} ")
    stdout.write("\n")

    let legalMoves = getLegalMoves(position, player)

    echo "Legal moves:"
    for legalMove in legalMoves:
      stdout.write(legalMove.toString)
      stdout.write(" ")
    stdout.write("\n")

    if isGameOver(position, player):
      let materialScore = materialScore(position)

      if materialScore > 0:
        echo "Black wins!"
      elif materialScore < 0:
        echo "White wins!"
      else:
        echo "It's a draw!"
      break

    elif legalMoves.len < 1:
      echo "No legal move. Enter \"p\" to pass."

    stdout.write("\n")
    stdout.write("> ")
    let input = string(stdin.readline)

    if input == "q" or input == "quit":
      isPlaying = false

    elif (input == "p" or input == "pass") and legalMoves.len == 0:
      player *= -1

    elif (input == "m" or input == "move") and legalMoves.len > 0:
      let move = getBestMove(position, player, depth)
      position = makeMove(position, player, move)
      player *= -1
      moveHistory.add(move.toString)

    elif match(input, re"depth [0-9]{1,2}$"):
      depth = parseInt(findAll(input, re"[0-9]{1,2}$")[0])
      echo fmt"Depth set to {depth}. Press Enter to continue"
      discard stdin.readline

    elif match(input, re"^[a-h][1-8]"):
      let move = input.fromString
      let newPosition = makeMove(position, player, move)

      if isLegalMove(position, player , move):
        player *= -1
        moveHistory.add(move.toString)

      position = newPosition

main()
